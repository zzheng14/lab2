/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author ericzheng
 *
 */
public class Celsius extends Temperature 
{ 
	 public Celsius(float t) 
	 { 
		 super(t);
	 } 
	 public String toString() 
	 { 
		 // TODO: Complete this method 
		 
		 return "The Celsius is "+ this.getValue(); 
	 }
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		return this;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		float f;
		f = (this.getValue()) * 9;
		f = f / 5;
		f = f + 32;
		return new Fahrenheit(f);
	} 
} 

